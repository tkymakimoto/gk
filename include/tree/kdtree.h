/*
 * kdtree.h
 *
 *  Created on: 2017/10/14
 *      Author: tmakimoto
 */

#ifndef TREE_KDTREE_H_
#define TREE_KDTREE_H_

#include "../gkdef.h"

#include <vector>
#include <array>

namespace gk {

template<std::size_t _N>
struct node {
	node<_N>* panret;
	std::array<node<_N>, _N> children;
};

/**
 * A node to be able to change number of children.
 */
template<>
struct node<__SIZE_MAX__> {

};

template<std::size_t _Dimension>
class kdtree {

};

}  // namespace gk

#endif /* TREE_KDTREE_H_ */
