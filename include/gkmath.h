/*
 * gkmath.h
 *
 *  Created on: 2018/03/17
 *      Author: tmakimoto
 */

#ifndef INCLUDE_GKMATH_H_
#define INCLUDE_GKMATH_H_

#include <cmath>

#include "gkdef.h"
#include "gkfunctional.h"

namespace gk {

template<typename _T>
_T sqrt(const product<_T, _T>& x) {
	const _T nx = x / _T(GK_FLOAT_ONE);
	return std::sqrt(nx);
}

}  // namespace gk
#endif /* INCLUDE_GKMATH_H_ */
