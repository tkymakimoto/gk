/*
 * test_aabb.cpp
 *
 *  Created on: 2017/10/01
 *      Author: tmakimoto
 */

#include <iostream>
#include <Eigen/Core>
#include <gkaabb.h>
#include <gkprimitive.h>

int main(int argc, char** argv) {
	gk::vector<double, gk::GK::GK_2D> a(0.0, 0.0);
	gk::vector<double, gk::GK::GK_2D> b(1.0, 2.0);

	gk::segment<double, gk::GK::GK_2D> seg(a, b);

	gk::aabb<double, gk::GK::GK_2D> bound = gk::make_aabb(seg);

	return 0;
}

